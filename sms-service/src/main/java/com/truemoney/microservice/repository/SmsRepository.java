package com.truemoney.microservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.truemoney.microservice.entities.SmsProvider; 

@Repository
public interface SmsRepository extends JpaRepository<SmsProvider, Long> {

}
