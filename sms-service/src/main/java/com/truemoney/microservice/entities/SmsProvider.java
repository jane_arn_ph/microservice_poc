package com.truemoney.microservice.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "l_SmsProvider")
public class SmsProvider {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long smsProviderId;
	private String name;
	private String smsFrom;
	private String apiKey;
	private String apiSecret;
	private String smsUrl;
	private boolean smsUrlSecurity;
	private boolean isAcitve;
	private int createdBy;
	private Date createdTime;
	private int modifiedBy;
	private Date modifiedTime;
	
	/**
	 * @return the smsProviderId
	 */
	public long getSmsProviderId() {
		return smsProviderId;
	}
	/**
	 * @param smsProviderId the smsProviderId to set
	 */
	public void setSmsProviderId(long smsProviderId) {
		this.smsProviderId = smsProviderId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the smsFrom
	 */
	public String getSmsFrom() {
		return smsFrom;
	}
	/**
	 * @param smsFrom the smsFrom to set
	 */
	public void setSmsFrom(String smsFrom) {
		this.smsFrom = smsFrom;
	}
	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}
	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	/**
	 * @return the apiSecret
	 */
	public String getApiSecret() {
		return apiSecret;
	}
	/**
	 * @param apiSecret the apiSecret to set
	 */
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}
	/**
	 * @return the smsUrl
	 */
	public String getSmsUrl() {
		return smsUrl;
	}
	/**
	 * @param smsUrl the smsUrl to set
	 */
	public void setSmsUrl(String smsUrl) {
		this.smsUrl = smsUrl;
	}
	/**
	 * @return the smsUrlSecurity
	 */
	public boolean isSmsUrlSecurity() {
		return smsUrlSecurity;
	}
	/**
	 * @param smsUrlSecurity the smsUrlSecurity to set
	 */
	public void setSmsUrlSecurity(boolean smsUrlSecurity) {
		this.smsUrlSecurity = smsUrlSecurity;
	}
	/**
	 * @return the isAcitve
	 */
	public boolean isAcitve() {
		return isAcitve;
	}
	/**
	 * @param isAcitve the isAcitve to set
	 */
	public void setAcitve(boolean isAcitve) {
		this.isAcitve = isAcitve;
	}
	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdTime
	 */
	public Date getCreatedTime() {
		return createdTime;
	}
	/**
	 * @param createdTime the createdTime to set
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	/**
	 * @return the modifiedBy
	 */
	public int getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedTime
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}
	/**
	 * @param modifiedTime the modifiedTime to set
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	
}
