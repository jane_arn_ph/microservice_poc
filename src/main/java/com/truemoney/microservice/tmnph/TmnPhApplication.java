package com.truemoney.microservice.tmnph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmnPhApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmnPhApplication.class, args);
	}
}
